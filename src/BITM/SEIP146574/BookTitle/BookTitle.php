<?php
namespace App\BookTitle;

use PDO;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class BookTitle extends DB
{
    public $id;
    public $book_title;
    public $author_name;
    public function __construct()
    {
        parent::__construct();
    }
    public function setData($postVariableData=Null)
    {
        if(array_key_exists('id',$postVariableData))
        {
            $this->id=$postVariableData['id'];
        }
        if(array_key_exists('book_title',$postVariableData))
        {
            $this->book_title=$postVariableData['book_title'];
        }
        if(array_key_exists('author_name',$postVariableData))
        {
            $this->author_name=$postVariableData['author_name'];
        }
    }
    public function store()
    {
    $arrData =array($this->book_title,$this->author_name);

    $sql="INSERT INTO book_title(book_title,author_name) VALUES (?,?)";

    $STH = $this->DBH->prepare($sql);

    $result = $STH->execute($arrData);

    if($result)
        Message::message("Success!Data has been inserted successfully");
    else
        Message::message("Failed!Data has been inserted successfully");

        Utility::redirect('create.php');
    }

    //end of store
    public function index($fetchMode='ASSOC')
    {
        $sql = "SELECT * from book_title where is_deleted = 0 ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if (substr_count($fetchMode, 'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData = $STH->fetchAll();
        return $arrAllData;
    }
    // end of index();
    public function view($fetchMode='ASSOC')
    {

        $STH = $this->DBH->query('SELECT * from book_title where id='.$this->id);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;
    }
    public function update()
    {
        $arrData =array($this->book_title,$this->author_name);
        $sql = "UPDATE  book_title set book_title =?,author_name=? where id=".$this->id;

        $STH = $this->DBH->prepare($sql);
        $STH->execute($arrData);
        Utility::redirect('index.php');

    }
    public function delete()
    {
        $sql="Delete from book_title where id=".$this->id;

        $STH = $this->DBH->prepare($sql);
        $result=$STH->execute();
        if($result)
            Message::message("Success!Data has been deleted successfully");
        else
            Message::message("Failed!Data has been deleted successfully");

        Utility::redirect('index.php');
    }
    public function trash()
    {
        $arrData =array($this->book_title,$this->author_name);
        $sql = "UPDATE  book_title set is_deleted=1 where id=".$this->id;

        $STH = $this->DBH->prepare($sql);
        $STH->execute($arrData);

        Utility::redirect('index.php');

    }
    public function trashList($fetchMode='ASSOC')
    {
        $sql = "SELECT * from book_title where is_deleted = 1 ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if (substr_count($fetchMode, 'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData = $STH->fetchAll();
        return $arrAllData;
    }
    public function restore()
    {
        $arrData =array($this->book_title,$this->author_name);
        $sql = "UPDATE  book_title set is_deleted=0 where id=".$this->id;

        $STH = $this->DBH->prepare($sql);
        $STH->execute($arrData);
        Utility::redirect('trashList.php');
    }
    public function indexPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from book_title  WHERE is_deleted = 0 LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }// end of indexPaginator();



    public function trashedPaginator($page=0,$itemsPerPage=3){

        $start = (($page-1) * $itemsPerPage);

        $sql = "SELECT * from book_title  WHERE is_deleted <> 0 LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;

    }// end of trashedPaginator();




    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )  $sql = "SELECT * FROM `book_title` WHERE `is_deleted` =0 AND (`book_title` LIKE '%".$requestArray['search']."%' OR `author_name` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byTitle']) && !isset($requestArray['byAuthor']) ) $sql = "SELECT * FROM `book_title` WHERE `is_deleted` =0 AND `book_title` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )  $sql = "SELECT * FROM `book_title` WHERE `is_deleted` =0 AND `author_name` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData = $STH->fetchAll();

        return $allData;
    }
    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();
        $sql = "SELECT * FROM `book_title` WHERE `is_deleted` =0";

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        // for each search field block start
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->book_title);
        }

        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);

        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->book_title);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->author_name);
        }
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $allData= $STH->fetchAll();
        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->author_name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }
    public function trashSelected($IDs = Array())
    {

        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $sql = "UPDATE `book_title` SET `is_deleted` = 0 WHERE `id` IN(" . $ids . ")";

            $STH = $this->DBH->prepare($sql);
            $result= $STH->execute();

            if ($result) {
                Message::message("
                  <div class=\"alert alert-success\">
                        <strong>Trashed!</strong> Selected Data has been trashed successfully.
                  </div>");
                Utility::redirect($_SERVER['HTTP_REFERER']);
            } else {
                Message::message("
                  <div class=\"alert alert-danger\">
                       <strong>Failed!</strong> Selected Data has not been trashed successfully.
                 </div>");
                Utility::redirect($_SERVER['HTTP_REFERER']);
            }
        }
        else {

            Message::message("
               <div class=\"alert alert-info\">
                   <strong>Empty Selection!</strong> No data has been selected.
               </div>");
            Utility::redirect($_SERVER['HTTP_REFERER']);
        }
    }




    public function deleteMultiple($IDs = Array())
    {


        if ((is_array($IDs)) && (count($IDs > 0))) {
            $ids = implode(",", $IDs);
            $sql = "DELETE FROM `book_title` WHERE `id`  IN(" . $ids . ")";

            $STH = $this->DBH->prepare($sql);
            $result= $STH->execute();

            if ($result) {
                Message::message("
                 <div class=\"alert alert-info\">
                      <strong>Deleted!</strong> Selected Data has been deleted successfully.
                 </div>");
                Utility::redirect($_SERVER['HTTP_REFERER']);
            } else {
                Message::message("
               <div class=\"alert alert-info\">
                   <strong>Deleted!</strong> Selected Data has not been deleted successfully.
               </div>");
                Utility::redirect($_SERVER['HTTP_REFERER']);
            }
        }
        else {

            Message::message("
               <div class=\"alert alert-info\">
                   <strong>Empty Selection!</strong> No data has been selected.
               </div>");

            Utility::redirect($_SERVER['HTTP_REFERER']);
        }

    }

}
//$objBookTitle=new BookTitle();