<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;

if(!isset($_SESSION))session_start();
echo Message::getMessage();


use App\City\City;

$objCity=new City;
$objCity->setData($_GET);

$oneData=$objCity->view("obj");


?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>City Edit</title>

    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../resource/assets/css/form-elements.css">
    <link rel="stylesheet" href="../../../resource/assets/css/style.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="../../../resource/assets/js/html5shiv.js"></script>
    <script src="../../../resource/assets/js/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<!-- Top content -->
<div class="top-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 form-box">
                <div class="form-top">
                    <div class="form-top-left">
                        <h3>City -Update</h3>
                        <p>Update Name and City:</p>
                    </div>
                    <div class="form-top-right">
                        <i class="fa fa-home"></i>
                    </div>
                </div>
                <div class="form-bottom">
                    <form role="form" action="update.php" method="post" class="login-form">
                        <div class="form-group">
                            <input type="hidden" name="id" value="<?php echo $oneData->id?>">
                            <label class="" for="name">Name :</label>
                            <input type="text" name="name" value="<?php echo $oneData->name?>" class="form-name form-control" id="form-name">
                        </div>
                        <div class="form-group">
                                <label class="" for="city_name">City Name</label>
                            <select name="city_name" class="form-control">
                                <option><?php echo $oneData->city_name ?></option>
                                <option  value="Dhaka">Dhaka</option>
                                <option  value="Chittagong">Chittagong</option>
                                <option  value="Noakhali">Noakhali</option>
                                <option  value="Comilla">Comilla</option>
                                <option  value="Cox's Bazar">Cox's Bazar</option>
                                <option  value="Khulna">Khulna</option>
                                <option  value="Faridpur">Faridpur</option>
                                <option  value="Jamalpur">Jamalpur</option>
                                <option  value="Feni">Feni</option>
                                <option  value="Sylhet">Sylhet</option>
                                <option  value="Barishal">Barishal</option>
                                <option  value="Rajshahi">Rajshahi</option>
                                <option  value="Rangpur">Rangpur</option>
                                <option  value="Gazipur">Gazipur</option>
                            </select>
                  </div>
                        <button type="submit" class="btn">Update</button>

                    </form>
                </div>
            </div>
        </div>

    </div>

</div>


<!-- Javascript -->
<script src="../../../resource/assets/js/jquery-1.11.1.min.js"></script>
<script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../../../resource/assets/js/jquery.backstretch.min.js"></script>
<script src="../../../resource/assets/js/scripts.js"></script>

<!--[if lt IE 10]>
<script src="../../../resource/assets/js/placeholder.js"></script>
<![endif]-->
</body>

</html>