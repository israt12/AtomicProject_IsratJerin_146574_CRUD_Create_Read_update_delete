<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;

if(!isset($_SESSION))session_start();
echo "<div id =\"message\">". Message::message()."</div>";

?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>City</title>

    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../resource/assets/css/form-elements.css">
    <link rel="stylesheet" href="../../../resource/assets/css/style.css">


    <script src="../../../resource/assets/js/html5shiv.js"></script>
    <script src="../../../resource/assets/js/respond.min.js"></script>
    <script type="text/javascript" src="../../../resource/assets/bootstrap/js/jquery.js"></script>

    <![endif]-->
</head>

<body">

<!-- Top content -->
<div class="top-content">
        <div class="container">
            <div class="row">
                <a href='index.php'><button class='btn btn-success'>Active List</button></a>
                <a href='trashList.php'><button class='btn btn-danger'>Trashed List</button></a>
                <div class="col-sm-6 col-sm-offset-3 form-box">
                    <div class="form-top">
                        <div class="form-top-left">
                            <h3>City</h3>
                            <p>Enter Name and Select a city:</p>
                        </div>
                        <div class="form-top-right">
                            <i class="fa fa-home"></i>
                        </div>
                    </div>
                    <div class="form-bottom">
                        <form role="form" action="store.php" method="post" class="login-form">
                            <div class="form-group">
                                <label class="" for="name">Name :</label>
                                <div class="input-group">
                                    <span class="input-group-addon" id="basic-addon1">@</span>
                                <input type="text" name="name" placeholder="name..." class="form-name form-control" id="form-name">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="" for="city_name">City Name</label>
                                <select name="city_name" class="form-control">
                                    <option   value="Dhaka">Dhaka</option>
                                    <option  value="Chittagong">Chittagong</option>
                                    <option  value="Noakhali">Noakhali</option>
                                    <option  value="Comilla">Comilla</option>
                                    <option  value="Cox's Bazar">Cox's Bazar</option>
                                    <option  value="Khulna">Khulna</option>
                                    <option  value="Faridpur">Faridpur</option>
                                    <option  value="Jamalpur">Jamalpur</option>
                                    <option  value="Feni">Feni</option>
                                    <option  value="Sylhet">Sylhet</option>
                                    <option  value="Barishal">Barishal</option>
                                    <option  value="Rajshahi">Rajshahi</option>
                                    <option  value="Rangpur">Rangpur</option>
                                    <option  value="Gazipur">Gazipur</option>
                                </select><br>

                            <button type="submit" class="btn">Create!</button>
                        </form>
                    </div>
                </div>
            </div>

        </div>

</div>


<!-- Javascript -->
<script src="../../../resource/assets/js/jquery-1.11.1.min.js"></script>
<script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
<script src="../../../resource/assets/js/jquery.backstretch.min.js"></script>
<script src="../../../resource/assets/js/scripts.js"></script>

<!--[if lt IE 10]>
<script src="../../../resource/assets/js/placeholder.js"></script>
<![endif]-->
<img src="../../../resource/assets/img/backgrounds/City%201.jpg" style="position: absolute; margin: 0px; padding: 0px; border: none; width: 100%; height: 100%; z-index: -999999; left: -1px; top: 0px;">
</body>

</html>
<script>
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(1200).fadeOut();
</script>