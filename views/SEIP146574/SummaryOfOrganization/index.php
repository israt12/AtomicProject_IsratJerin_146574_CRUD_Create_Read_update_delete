<?php
require_once("../../../vendor/autoload.php");

use App\SummaryOfOrganization\SummaryOfOrganization;
use App\Utility\Utility;
$objSummaryOfOrganization=new SummaryOfOrganization;

$allData=$objSummaryOfOrganization->index("obj");

################## search  block1 start ##################
if(isset($_REQUEST['search']) )$allData =  $objSummaryOfOrganization->search($_REQUEST);

$availableKeywords=$objSummaryOfOrganization->getAllKeywords();
$comma_separated_keywords= '"'.implode('","',$availableKeywords).'"';
################## search  block1 end ##################
######################## pagination code block#1 of 2 start ######################################
$recordCount= count($allData);


if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;
$_SESSION['Page']= $page;

if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;

$pages = ceil($recordCount/$itemsPerPage);
$someData = $objSummaryOfOrganization->indexPaginator($page,$itemsPerPage);

$serial = (($page-1) * $itemsPerPage) +1;

####################### pagination code block#1 of 2 end #########################################


################## search  block2 start ##################

if(isset($_REQUEST['search']) ) {
  $someData = $objSummaryOfOrganization->search($_REQUEST);
  $serial = 1;
}
################## search  block2 end ##################


?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Index</title>
  <meta name="description" content="description">
  <meta name="author" content="DevOOPS">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="../../../resource/assets1/plugins/bootstrap/bootstrap.css" rel="stylesheet">
  <link href="../../../resource/assets1/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet">
  <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
  <link href='http://fonts.googleapis.com/css?family=Righteous' rel='stylesheet' type='text/css'>
  <link href="../../../resource/assets1/plugins/fancybox/jquery.fancybox.css" rel="stylesheet">
  <link href="../../../resource/assets1/plugins/fullcalendar/fullcalendar.css" rel="stylesheet">
  <link href="../../../resource/assets1/plugins/xcharts/xcharts.min.css" rel="stylesheet">
  <link href="../../../resource/assets1/plugins/select2/select2.css" rel="stylesheet">
  <link href="../../../resource/assets1/plugins/justified-gallery/justifiedGallery.css" rel="stylesheet">
  <link href="../../../resource/assets1/css/style_v1.css" rel="stylesheet">
  <link href="../../../resource/assets1/plugins/chartist/chartist.min.css" rel="stylesheet">
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="http://getbootstrap.com/docs-assets/js/html5shiv.js"></script>
  <script src="http://getbootstrap.com/docs-assets/js/respond.min.js"></script>
  <link href="../../../resource/assets1/css/round-about.css" rel="stylesheet">



  <script language="JavaScript" type="text/javascript">
    function checkDelete()
    {
      return confirm("Are you sure you want to delete");
    }
  </script>
  <![endif]-->
  <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/jquery-ui.css">
  <script src="../../../resource/assets/bootstrap/js/jquery-1.12.4.js"></script>
  <script src="../../../resource/assets/bootstrap/js/jquery-ui.js"></script>

  <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../../resource/assets/font-awesome/css/font-awesome.min.css">
  <script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="../../../resource/assets/js/jquery.backstretch.min.js"></script>


  <style>
    table{
      text-align: center;
      margin :50px auto;
      background-color:rgba(183, 179, 179, 0.15);
    }
    th{
      text-align: center;
      height: 32px;
      color: #5e90c3;
      font-family: serif;
      font-size: 19px;
    }
    td,tr{
      padding: 20px;
      text-align: center;
    }
    .m{
      margin-top: -70px;
    }
  </style>
</head>
<body>
<!--Start Header-->
<div class="whole1" style="width:1335px">
<header class="navbar">
  <div class="container-fluid expanded-panel">
    <div class="row">
      <div id="logo" class="col-xs-12 col-sm-2">
        <a href="index.php">Atomic Project</a>
      </div>
      <div id="top-panel" class="">
        <div class="row">
          <div class="">
            <a href='create.php' style="margin-left: 790px;"><button class='btn btn-success'>Add New</button></a>
            <a href='trashList.php'><button class='btn btn-primary'>Trashed List</button></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>
<!--End Header-->
<!--Start Container-->
<div id="main" style="margin-top: -21px" class="container-fluid">
  <div class="row">
    <div id="sidebar-left" class="col-xs-2 col-sm-2">
      <ul class="nav main-menu">
        <li>
          <a href="../index.php" class="">
            <i class="fa fa-dashboard"></i>
            <span class="hidden-xs">Dashboard</span>
          </a>
        </li>
        <li class="dropdown">
          <a href="../BookTitle/create.php" class="">
            <i class="fa fa-book"></i>
            <span class="hidden-xs">Book Title</span>
          </a>
        </li>
        <li class="dropdown">
          <a href="../BirthDay/create.php" class="">
            <i class="fa fa-birthday-cake"></i>
            <span class="hidden-xs">BirthDay</span>
          </a>
        </li>
        <li class="dropdown">
          <a href="../City/create.php" class="">
            <i class="fa fa-home"></i>
            <span class="hidden-xs">City</span>
          </a>
        </li>
        <li class="dropdown">
          <a href="../Email/create.php" class="">
            <i class="fa fa-credit-card"></i>
            <span class="hidden-xs">Email</span>
          </a>
        </li>
        <li class="dropdown">
          <a href="../Gender/create.php" class="">
            <i class="fa fa-square-o"></i>
            <span class="hidden-xs">Gender</span>
          </a>
        </li>
        <li class="dropdown">
          <a href="create.php" class="dropdown-toggle">
            <i class="fa fa-map-marker"></i>
            <span class="hidden-xs">Hobbies</span>
          </a>

        </li>
        <li class="dropdown">
          <a href="../ProfilePicture/create.php" class="">
            <i class="fa fa-picture-o"></i>
            <span class="hidden-xs">Profile Picture</span>
          </a>
        </li>
        <li>
          <a class="" href="create.php">
            <i class="fa fa-font"></i>
            <span class="hidden-xs">Summary of organization</span>
          </a>
        </li>
    </div>
    <!--Start Content-->
    <div id="content" class="col-xs-12 col-sm-10">

      <div class="container">
        <head>

          <img src="../../../resource/assets/img/backgrounds/table.jpg" style="position: absolute; margin: 0px; padding: 0px; border: none; width: 100%; height:100% ;  z-index: -999999; top: 0px;">


        </head>
        <form id="searchForm" action="index.php"  method="get" style="margin-top: 50px">
          <input type="text" value="" id="searchID" name="search" placeholder="Search" width="60" ><br>
          <input type="checkbox"  name="byName"   checked  >By Name
          <input type="checkbox"  name="bySummaryOfOrganization"  checked >By Summary Of Organization
          <input hidden type="submit" class="btn-primary" value="search">
        </form>
        <div class="m"  style="margin-left: 650px">
          <td>
            <a href="pdf.php" class="btn btn-primary" role="button">Download as PDF</a>
            <a href="xl.php" class="btn btn-primary" role="button">Download as XL</a>
            <a href="email.php?list=1" class="btn btn-primary" role="button">Email to friend</a>
          </td>
        </div>

        <?php
        $serial=1;
        echo "<table class='table table-striped' style='width: 102%;'>";
        echo "<th> Serial No</th><th > ID</th><th>Name</th><th>Gender</th><th>Action </th>";
        foreach($someData as $oneData)

        {
          echo "<tr style='height: 40px'>";
          echo "<td> $serial</td>";
          echo "<td> $oneData->id</td>";
          echo "<td> $oneData->name</td>";
          echo "<td style='width:380px;'> $oneData->organization_summary</td>";
          echo"
  <td>
        <a href='view.php?id=$oneData->id'><button class='btn btn-info'><img src='../../../resource/assets/img/backgrounds/v.png' width='15'  height='15' > View</button></a>
        <a href='edit.php?id=$oneData->id'><button class='btn btn-success'><img src='../../../resource/assets/img/backgrounds/editt.png' width='15'  height='15' > Edit</button></a>
        <a href='trash.php?id=$oneData->id'><button class='btn btn-primary'><img src='../../../resource/assets/img/backgrounds/trash.jpg' width='15'  height='15' > Trash</button></a>
        <a href='delete.php?id=$oneData->id'onclick='return checkDelete()'><button class='btn btn-danger'><img src='../../../resource/assets/img/backgrounds/del.png' width='15' height='15'> Delete</button></a>
        <a href='email.php?id=$oneData->id'><button class='btn btn-info'>Email</button></a>

   </td>
  ";

          echo "</tr>";
          $serial++;
        }
        echo "</table>";
        ?>

        <!--  ######################## pagination code block#2 of 2 start ###################################### -->
        <div style="margin-left: 330px" class="container">
          <ul class="pagination">

            <?php
            $pageMinusOne=$page-1;   //$pageMinus=$page-1;
            $pagePlusOne=$page+1;    //$pageMinus=$page+1;
            if($page>$pages) Utility::redirect("index.php?Page=$pages");

            if($page>1)echo "<li><a href='index.php?Page=$pageMinusOne'>" . "Previous" . '</a></li>';
            for($i=1;$i<=$pages;$i++)
            {
              if($i==$page) echo '<li class="active"><a href="">'. $i . '</a></li>';
              else  echo "<li><a href='?Page=$i'>". $i . '</a></li>';

            }
            if($page<$pages)echo "<li><a href='index.php?Page=$pagePlusOne'>" . "Next" . '</a></li>';
            ?>

            <select  class="form-control"  name="ItemsPerPage" id="ItemsPerPage" onchange="javascript:location.href = this.value;" >
              <?php
              if($itemsPerPage==3 ) echo '<option value="?ItemsPerPage=3" selected >Show 3 Items Per Page</option>';
              else echo '<option  value="?ItemsPerPage=3">Show 3 Items Per Page</option>';

              if($itemsPerPage==4 )  echo '<option  value="?ItemsPerPage=4" selected >Show 4 Items Per Page</option>';
              else  echo '<option  value="?ItemsPerPage=4">Show 4 Items Per Page</option>';

              if($itemsPerPage==5 )  echo '<option  value="?ItemsPerPage=5" selected >Show 5 Items Per Page</option>';
              else echo '<option  value="?ItemsPerPage=5">Show 5 Items Per Page</option>';

              if($itemsPerPage==6 )  echo '<option  value="?ItemsPerPage=6"selected >Show 6 Items Per Page</option>';
              else echo '<option  value="?ItemsPerPage=6">Show 6 Items Per Page</option>';

              if($itemsPerPage==10 )   echo '<option  value="?ItemsPerPage=10"selected >Show 10 Items Per Page</option>';
              else echo '<option  value="?ItemsPerPage=10">Show 10 Items Per Page</option>';

              if($itemsPerPage==15 )  echo '<option  value="?ItemsPerPage=15"selected >Show 15 Items Per Page</option>';
              else    echo '<option  value="?ItemsPerPage=15">Show 15 Items Per Page</option>';
              ?>
            </select>
          </ul>
        </div>
        <!--  ######################## pagination code block#2 of 2 end ###################################### -->
      </div>

    </div>

  </div>
  <!--End Content-->
</div>
</div>


</body>
</html>

<script>

  $(function() {
    var availableTags = [

      <?php
      echo $comma_separated_keywords;
      ?>
    ];
    // Filter function to search only from the beginning of the string
    $( "#searchID" ).autocomplete({
      source: function(request, response) {

        var results = $.ui.autocomplete.filter(availableTags, request.term);

        results = $.map(availableTags, function (tag) {
          if (tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0) {
            return tag;
          }
        });

        response(results.slice(0, 15));

      }
    });


    $( "#searchID" ).autocomplete({
      select: function(event, ui) {
        $("#searchID").val(ui.item.label);
        $("#searchForm").submit();
      }
    });


  });

</script>
<script src="../../../resource/assets1/plugins/jquery/jquery.min.js"></script>
<script src="../../../resource/assets1/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../../../resource/assets1/plugins/bootstrap/bootstrap.min.js"></script>
<script src="../../../resource/assets1/plugins/justified-gallery/jquery.justifiedGallery.min.js"></script>
<script src="../../../resource/assets1/plugins/tinymce/tinymce.min.js"></script>
<script src="../../../resource/assets1/plugins/tinymce/jquery.tinymce.min.js"></script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!--<script src="http://code.jquery.com/jquery.js"></script>-->
<script src="../../../resource/assets1/plugins/jquery/jquery.min.js"></script>
<script src="../../../resource/assets1/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../../../resource/assets1/plugins/bootstrap/bootstrap.min.js"></script>
<script src="../../../resource/assets1/plugins/justified-gallery/jquery.justifiedGallery.min.js"></script>
<script src="../../.../resource/assets1/plugins/tinymce/tinymce.min.js"></script>
<script src="../../../resource/assets1/plugins/tinymce/jquery.tinymce.min.js"></script>
<!-- All functions for this theme + document.ready processing -->
</body>
</html>
