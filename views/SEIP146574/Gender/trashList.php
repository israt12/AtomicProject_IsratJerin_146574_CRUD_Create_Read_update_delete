
<head>
    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/assets/font-awesome/css/font-awesome.min.css">
    <script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../resource/assets/js/jquery.backstretch.min.js"></script>
    <img src="../../../resource/assets/img/backgrounds/table.jpg" style="position: absolute; margin: 0px; padding: 0px; border: none; width: 100%; height: 100%;  z-index: -999999; top: 0px;">

    <style>
        table{
            text-align: center;
            margin :50px auto;
            background-color:rgba(183, 179, 179, 0.15);
            width: 700px;
        }
        th{
            text-align: center;
            height: 32px;
            color: #435d77;
            font-family: serif;
            font-size: 19px;
        }
        td,tr{
            padding: 20px;
            text-align: center;
        }
    </style>
    <script language="JavaScript" type="text/javascript">
        function checkDelete()
        {
            return confirm("Are you sure you want to delete");
        }
    </script>
</head><?php
require_once("../../../vendor/autoload.php");

use App\Gender\Gender;
use App\Message\Message;

if(!isset($_SESSION))session_start();
echo "<div id =\"message\">". Message::message()."</div>";

$objGender=new Gender();

$objGender->setData($_GET);
$allData=$objGender->trashList("obj");
?>
<a href='index.php' style="margin-left: 1115px;"><button class='btn btn-success'>Back Index page</button></a>
<h4 style="margin-left: 180px;">Trash List:</h4>
<?php
$serial=1;
echo "<table class='table table-striped'>";
echo "<th> Serial No</th><th > ID</th><th>Name</th><th>Gender</th><th>Action</th>";
foreach($allData as $oneData)

{
    echo "<tr style='height: 40px'>";
    echo "<td> $serial</td>";
    echo "<td> $oneData->id</td>";
    echo "<td> $oneData->name</td>";
    echo "<td> $oneData->sex</td>";
    echo"
  <td>
        <a href='restore.php?id=$oneData->id'><button class='btn btn-success'><img src='../../../resource/assets/img/backgrounds/restore.jpg' width='15' height='15'> Restore</button></a>
        <a href='delete.php?id=$oneData->id'onclick='return checkDelete()'><button class='btn btn-danger'><img src='../../../resource/assets/img/backgrounds/del.png' width='15' height='15'> Delete</button></a>

   </td>
  ";

    echo "</tr>";
    $serial++;
}
echo "</table>";
