
<head>
    <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/assets/font-awesome/css/font-awesome.min.css">
    <script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../resource/assets/js/jquery.backstretch.min.js"></script>
    <img src="../../../resource/assets/img/backgrounds/table.jpg" style="position: absolute; margin: 0px; padding: 0px; border: none; width: 100%; height: 100%;  z-index: -999999; top: 0px;">

    <style>
        table{
            text-align: center;
            margin :50px auto;
            background-color:rgba(183, 179, 179, 0.15);
            width: 700px;
        }
        th{
            text-align: center;
            height: 32px;
            color: #435d77;
            font-family: serif;
            font-size: 19px;
        }
        td,tr{
            padding: 20px;
            text-align: center;
        }
    </style>
    <script language="JavaScript" type="text/javascript">
        function checkDelete()
        {
            return confirm("Are you sure you want to delete");
        }
    </script>
</head><?php
require_once("../../../vendor/autoload.php");

use App\BookTitle\BookTitle;
use App\Utility\Utility;


$objBookTitle=new BookTitle;

$objBookTitle->setData($_GET);
$allData=$objBookTitle->trashList("obj");




$recordCount= count($allData);

if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;
$_SESSION['Page']= $page;

if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;

$pages = ceil($recordCount/$itemsPerPage);
$someData = $objBookTitle->trashedPaginator($page,$itemsPerPage);

$serial = (($page-1) * $itemsPerPage) +1;

?>
<a href='index.php' style="margin-left: 1115px;"><button class='btn btn-success'>Back Index page</button></a>
<h4 style="margin-left: 180px;">Trash List:</h4>
<?php
$serial=1;
echo "<table class='table table-striped'>";
echo "<th> Serial</th><th > Id</th><th> Book Title</th><th> Author Name</th><th>Action</th>";
foreach($someData as $oneData)

{
    echo "<tr style='height: 40px'>";
    echo "<td> $serial</td>";
    echo "<td> $oneData->id</td>";
    echo "<td> $oneData->book_title</td>";
    echo "<td> $oneData->author_name</td>";
    echo"
  <td>
        <a href='restore.php?id=$oneData->id'><button class='btn btn-success'><img src='../../../resource/assets/img/backgrounds/restore.jpg' width='15' height='15'> Restore</button></a>
        <a href='delete.php?id=$oneData->id'onclick='return checkDelete()'><button class='btn btn-danger'><img src='../../../resource/assets/img/backgrounds/del.png' width='15' height='15'> Delete</button></a>

   </td>
  ";
    echo "</tr>";
    $serial++;
}
echo "</table>";
?>

<!--  ######################## pagination code block#2 of 2 start ###################################### -->
<div style="margin-left: 330px" class="">
    <ul class="pagination">

        <?php
        $pageMinusOne=$page-1;   //$pageMinus=$page-1;
        $pagePlusOne=$page+1;    //$pageMinus=$page+1;
        if($page>$pages) Utility::redirect("trashList.php?Page=$pages");

        if($page>1)echo "<li><a href='trashList.php?Page=$pageMinusOne'>" . "Previous" . '</a></li>';
        for($i=1;$i<=$pages;$i++)
        {
            if($i==$page) echo '<li class="active"><a href="">'. $i . '</a></li>';
            else  echo "<li><a href='?Page=$i'>". $i . '</a></li>';

        }
        if($page<$pages)echo "<li><a href='trashList.php?Page=$pagePlusOne'>" . "Next" . '</a></li>';
        ?>

        <select  class="form-control"  name="ItemsPerPage" id="ItemsPerPage" onchange="javascript:location.href = this.value;" >
            <?php
            if($itemsPerPage==3 ) echo '<option value="?ItemsPerPage=3" selected >Show 3 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=3">Show 3 Items Per Page</option>';

            if($itemsPerPage==4 )  echo '<option  value="?ItemsPerPage=4" selected >Show 4 Items Per Page</option>';
            else  echo '<option  value="?ItemsPerPage=4">Show 4 Items Per Page</option>';

            if($itemsPerPage==5 )  echo '<option  value="?ItemsPerPage=5" selected >Show 5 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=5">Show 5 Items Per Page</option>';

            if($itemsPerPage==6 )  echo '<option  value="?ItemsPerPage=6"selected >Show 6 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=6">Show 6 Items Per Page</option>';

            if($itemsPerPage==10 )   echo '<option  value="?ItemsPerPage=10"selected >Show 10 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=10">Show 10 Items Per Page</option>';

            if($itemsPerPage==15 )  echo '<option  value="?ItemsPerPage=15"selected >Show 15 Items Per Page</option>';
            else    echo '<option  value="?ItemsPerPage=15">Show 15 Items Per Page</option>';
            ?>
        </select>
    </ul>
</div>
<!--  ######################## pagination code block#2 of 2 end ###################################### -->

