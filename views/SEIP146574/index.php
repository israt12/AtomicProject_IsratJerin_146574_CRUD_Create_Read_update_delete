<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Index</title>
    <meta name="description" content="description">
    <meta name="author" content="DevOOPS">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="../../resource/assets1/plugins/bootstrap/bootstrap.css" rel="stylesheet">
    <link href="../../resource/assets1/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet">
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Righteous' rel='stylesheet' type='text/css'>
    <link href="../../resource/assets1/plugins/fancybox/jquery.fancybox.css" rel="stylesheet">
    <link href="../../resource/assets1/plugins/fullcalendar/fullcalendar.css" rel="stylesheet">
    <link href="../../resource/assets1/plugins/xcharts/xcharts.min.css" rel="stylesheet">
    <link href="../../resource/assets1/plugins/select2/select2.css" rel="stylesheet">
    <link href="../../resource/assets1/plugins/justified-gallery/justifiedGallery.css" rel="stylesheet">
    <link href="../../resource/assets1/css/style_v1.css" rel="stylesheet">
    <link href="../../resource/assets1/plugins/chartist/chartist.min.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="http://getbootstrap.com/docs-assets/js/html5shiv.js"></script>
    <script src="http://getbootstrap.com/docs-assets/js/respond.min.js"></script>
    <link href="../../resource/assets1/css/round-about.css" rel="stylesheet">

    <![endif]-->
</head>
<body>
<!--Start Header-->
<header class="navbar">
    <div class="container-fluid expanded-panel">
        <div class="row">
            <div id="logo" class="col-xs-12 col-sm-2">
                <a href="index.php">Atomic Project</a>
            </div>
            <div id="top-panel" class="col-xs-12 col-sm-10">
                <div class="row">
                    <div class="col-xs-8 col-sm-4">
                        <div id="search">
                            <input type="text" placeholder="search"/>
                            <i class="fa fa-search"></i>
                        </div>
                    </div>
                    <div class="col-xs-4 col-sm-8 top-panel-right">
                        <a href="#" class="about">about</a>
                        <a href="index.php" class="style2"></a>
                        <ul class="nav navbar-nav pull-right panel-menu">
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle account" data-toggle="dropdown">
                                    <div class="avatar">
                                        <img src="../../resource/assets/img/backgrounds/10.jpg" class="img-circle" alt="avatar" />
                                    </div>
                                    <i class="fa fa-angle-down pull-right"></i>
                                    <div class="user-mini pull-right">
                                        <span class="welcome">Welcome,</span>
                                    </div>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-user"></i>
                                            <span>Profile</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="ajax/page_messages.html" class="ajax-link">
                                            <i class="fa fa-envelope"></i>
                                            <span>Messages</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="ajax/gallery_simple.html" class="ajax-link">
                                            <i class="fa fa-picture-o"></i>
                                            <span>Albums</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="ajax/calendar.html" class="ajax-link">
                                            <i class="fa fa-tasks"></i>
                                            <span>Tasks</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-cog"></i>
                                            <span>Settings</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-power-off"></i>
                                            <span>Logout</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!--End Header-->
<!--Start Container-->
<div id="main" class="container-fluid">
    <div class="row">
        <div id="sidebar-left" class="col-xs-2 col-sm-2">
            <ul class="nav main-menu">
                <li>
                    <a href="index.php" class="">
                        <i class="fa fa-dashboard"></i>
                        <span class="hidden-xs">Dashboard</span>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="BookTitle/create.php" class="">
                        <i class="fa fa-book"></i>
                        <span class="hidden-xs">Book Title</span>
                </a>
                </li>
                <li class="dropdown">
                    <a href="BirthDay/create.php" class="">
                        <i class="fa fa-birthday-cake"></i>
                        <span class="hidden-xs">BirthDay</span>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="City/create.php" class="">
                        <i class="fa fa-home"></i>
                        <span class="hidden-xs">City</span>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="Email/create.php" class="">
                        <i class="fa fa-credit-card"></i>
                        <span class="hidden-xs">Email</span>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="Gender/create.php" class="">
                        <i class="fa fa-square-o"></i>
                        <span class="hidden-xs">Gender</span>
                    </a>
                </li>
                <li class="dropdown">
                    <a href="Hobbies/create.php" class="dropdown-toggle">
                        <i class="fa fa-map-marker"></i>
                        <span class="hidden-xs">Hobbies</span>
                    </a>

                </li>
                <li class="dropdown">
                    <a href="ProfilePicture/create.php" class="">
                        <i class="fa fa-picture-o"></i>
                        <span class="hidden-xs">Profile Picture</span>
                    </a>
                </li>
                <li>
                    <a class="" href="SummaryOfOrganization/create.php">
                        <i class="fa fa-font"></i>
                        <span class="hidden-xs">Summary of organization</span>
                    </a>
                </li>
        </div>
        <!--Start Content-->
        <div id="content" class="col-xs-12 col-sm-10">

            <div class="container">

                <!-- Introduction Row -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Welcome !!
                            <small>It's Nice to Meet You!</small>
                        </h1>
                        <p>This is  my Atomic project.you can see the list of book title,birthday,city,email,gender,hobby,profile picture and summary of organization.</p>
                    </div>
                </div>

                <!-- Team Members Row -->
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="page-header">Our Title</h2>
                    </div>
                    <div class="col-lg-3 col-sm-6 text-center">
                        <a href="BookTitle/index.php">
                        <img class="img-circle img-responsive img-center" src="../../resource/assets/img/backgrounds/book.jpg" alt="" width="200px">
                      </a>
                        <h3>Book Title
                            <small>Index</small>
                        </h3>
                        <p></p>
                    </div>
                    <div class="col-lg-3 col-sm-6 text-center">
                        <a href="BirthDay/index.php">
                        <img class="img-circle img-responsive img-center" src="../../resource/assets/img/backgrounds/bb.jpg" alt=""width="200px">
                        </a>
                            <h3>Birthday
                            <small>Index</small>
                        </h3>
                        <p></p>
                    </div>
                    <div class="col-lg-3 col-sm-6 text-center">
                        <a href="Email/index.php">
                        <img class="img-circle img-responsive img-center" src="../../resource/assets/img/backgrounds/e.jpg" alt=""width="200px">
                         </a>
                        <h3>Email
                            <small>Index</small>
                        </h3>
                        <p></p>
                    </div>
                    <div class="col-lg-3 col-sm-6 text-center">
                        <a href="City/index.php">
                        <img class="img-circle img-responsive img-center" src="../../resource/assets/img/backgrounds/mm.jpg" alt="" width="200px">
                        </a>
                        <h3>City
                            <small>Index</small>
                        </h3>
                        <p></p>
                    </div>
                    </div>
                <div class="row">
                    <div class="col-lg-3 col-sm-6 text-center">
                        <a href="Gender/index.php">
                            <img class="img-circle img-responsive img-center" src="../../resource/assets/img/backgrounds/gg.jpg" alt=""width="200px">
                        </a>
                        <h3>Gender
                            <small>Index</small>
                        </h3>
                        <p></p>
                    </div>
                    <div class="col-lg-3 col-sm-6 text-center"  style="margin-left: -10px">
                        <a href="Hobbies/index.php">
                            <img class="img-circle img-responsive img-center" src="../../resource/assets/img/backgrounds/h.png" alt=""width="200px">
                        </a>
                        <h3>Hobbies
                            <small>Index</small>
                        </h3>
                        <p></p>
                    </div>
                    <div class="col-lg-3 col-sm-6 text-center">
                        <a href="ProfilePicture/index.php">
                            <img class="img-circle img-responsive img-center" src="../../resource/assets/img/backgrounds/ppp.png" alt="" width="200px">
                        </a>
                        <h3>Profile Picture
                            <small>Index</small>
                        </h3>
                        <p></p>
                    </div>
                    <div class="col-lg-3 col-sm-6 text-center">
                        <a href="SummaryOfOrganization/index.php">
                            <img class="img-circle img-responsive img-center" src="../../resource/assets/img/backgrounds/k.jpg" alt="" width="200px">
                        </a>
                        <h3>Summary of organization
                            <small>Index</small>
                        </h3>
                        <p></p>
                    </div>
            </div>
            </div>
                <hr>

                <!-- Footer -->
                <footer>
                    <div class="row" align="center">
                        <div class="col-lg-12">
                            <p>Copyright &copy; My Website 2016</p>
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </footer>

            </div>

        </div>
        <!--End Content-->
    </div>
</div>
<!--End Container-->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!--<script src="http://code.jquery.com/jquery.js"></script>-->
<script src="../../resource/assets1/plugins/jquery/jquery.min.js"></script>
<script src="../../resource/assets1/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../../resource/assets1/plugins/bootstrap/bootstrap.min.js"></script>
<script src="../../resource/assets1/plugins/justified-gallery/jquery.justifiedGallery.min.js"></script>
<script src="../../resource/assets1/plugins/tinymce/tinymce.min.js"></script>
<script src="../../resource/assets1/plugins/tinymce/jquery.tinymce.min.js"></script>
<!-- All functions for this theme + document.ready processing -->
</body>
</html>
