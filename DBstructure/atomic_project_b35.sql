-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 25, 2016 at 07:56 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomic_project_b35`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE `birthday` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `birthdate` date NOT NULL,
  `is_deleted` varchar(255) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `name`, `birthdate`, `is_deleted`) VALUES
(1, 'israt jerin', '1994-06-08', '0'),
(4, 'zakia sharmin', '1994-03-27', '0'),
(5, 'shimu', '2016-11-05', '1'),
(6, 'mahin', '2016-11-10', '1'),
(7, 'abida s', '2016-11-17', '0'),
(8, 'sultana', '2016-11-03', '1'),
(10, 'moni', '2016-11-11', '0'),
(11, 'tajkia', '2016-11-17', '0');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE `book_title` (
  `id` int(11) NOT NULL,
  `book_title` varchar(200) NOT NULL,
  `author_name` varchar(200) NOT NULL,
  `is_deleted` varchar(255) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_title`, `author_name`, `is_deleted`) VALUES
(1, 'Learning PHP, MySQL, and JavaScript', 'Robin Nixons', '0'),
(2, 'PHP Object-Oriented Solutions ', ' David Powers ', '0'),
(4, 'Ema', 'Ema', '0'),
(11, 'php', 'tajkia', '0'),
(12, 'html5', 'zakia', '1'),
(13, 'advanced php', 'shimu', '1'),
(14, 'bangla', 'mahin', '1'),
(15, 'bcxcv', 'bvcvc', '1'),
(16, 'english', 'tammanna', '0'),
(17, 'hjjhg', 'kjhgjh', '0'),
(18, 'sadsa', 'sadasd', '0'),
(19, 'saassd', 'asda', '0'),
(20, 'css', 'shifta', '0'),
(21, 'gjfjgf', 'fjjf', '0'),
(22, 'jhgjjh', 'fggf', '0');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `city_name` varchar(200) NOT NULL,
  `is_deleted` varchar(255) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `city_name`, `is_deleted`) VALUES
(1, 'israt', 'Cox''s Bazar', '0'),
(2, 'jerin', 'Jamalpur', '1'),
(4, 'shamima', 'Chittagong', '0'),
(7, 'ema', 'Dhaka', '1'),
(8, 'sanjida', 'Comilla', '0'),
(9, 'nusrat', 'Dhaka', '0'),
(10, 'tammanna', 'Chittagong', '0'),
(11, 'tushi', 'Jamalpur', '1'),
(12, 'moni', 'Khulna', '1'),
(13, 'shifta', 'Cox''s Bazar', '0'),
(14, 'tajki', 'Dhaka', '0'),
(15, 'saimon', 'Chittagong', '0'),
(16, 'mahin', 'Dhaka', '0'),
(17, 'tanjila', 'Sylhet', '0'),
(18, 'shimu', 'Dhaka', '0');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `is_deleted` varchar(255) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `name`, `email`, `is_deleted`) VALUES
(1, 'israt', 'jerinisrat.cse@gmail.com', '0'),
(2, 'jerin', 'israt751@gmail.com', '0'),
(4, 'tajkia', 'tajkia44@gmail.com', '1'),
(5, 'afrin', 'isratfdf@gmail.com', '0'),
(6, 'rubina', 'tamu@gmail.com', '0'),
(7, 'noushin', 'shjamu@gmail.com', '0'),
(8, 'sanjida', 'sanjida@gmail.com', '0'),
(9, 'abidaa', 'abida@gmail.com', '0'),
(10, 'shati', 'shati@gmail.com', '0'),
(11, 'tushi', 'tushi@gmail.com', '0'),
(12, 'shimu', 'shimu@gmail.com', '0'),
(13, 'shifta', 'shifta@gmail.com', '0');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `sex` varchar(200) NOT NULL,
  `is_deleted` varchar(255) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `name`, `sex`, `is_deleted`) VALUES
(1, 'israt', 'female', '0'),
(2, 'jerin', 'female', '0'),
(4, 'zakia', 'female', '1'),
(5, 'shifta', 'female', '0'),
(6, 'tajkia', 'female', '0'),
(7, 'shimu', 'female', '0'),
(8, 'tammanna\r\n', 'female', '1'),
(9, 'tushi\r\n', 'female', '0'),
(10, 'afrin', 'female', '1'),
(11, 'rubina', 'female', '0'),
(12, 'tasnia', 'female', '0');

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE `hobbies` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `hobby` varchar(200) NOT NULL,
  `is_deleted` varchar(255) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `name`, `hobby`, `is_deleted`) VALUES
(1, 'israt', 'reading book', '0'),
(2, 'jerin', 'gaming', '1'),
(3, 'shifta', 'HTML/CSS', '1'),
(5, 'ema', 'Reading,HTML/CSS', '0'),
(8, 'tajki', 'Gardening,HTML/CSS', '0'),
(9, 'mumu', 'HTML/CSS', '0'),
(10, 'nasrin', 'Gardening,Reading', '0'),
(11, 'akter', 'Gardening', '0'),
(12, 'sharmin', 'Gaming,Reading', '0'),
(13, 'tasnia', 'Gardening,HTML/CSS', '0'),
(14, 'mahin', 'Gaming', '1'),
(15, 'afrin', 'Gaming,Reading', '0'),
(16, 'aymon', 'Gardening,Reading', '0'),
(17, 'shimu', 'Reading,HTML/CSS', '0');

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE `profile_picture` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `image` varchar(200) NOT NULL,
  `is_deleted` varchar(255) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_picture`
--

INSERT INTO `profile_picture` (`id`, `name`, `image`, `is_deleted`) VALUES
(1, 'isratjgffdujhgfgjghhhhh', '1479699080aaassspture.JPG', '0'),
(2, 'jerin', '147991203614958743_315404618844232_15149227_n (1).png', '1'),
(4, 'Tasnia Mahmuda', '1479699512download.jpg', '0'),
(5, 'mahin', '1480091168Lighthouse.jpg', '0'),
(6, 'tajki', '1480091178Hydrangeas.jpg', '0'),
(7, 'shifta', '1480091185Tulips.jpg', '0'),
(8, 'sharmin', '1480091194Jellyfish.jpg', '1'),
(9, 'zakia', '1480091201Desert.jpg', '0'),
(10, 'afroja', '1480091212Chrysanthemum.jpg', '0'),
(11, 'sanjana', '1480091227Lighthouse.jpg', '0'),
(12, 'sujata', '1480091235Lighthouse.jpg', '1');

-- --------------------------------------------------------

--
-- Table structure for table `summary_of_organization`
--

CREATE TABLE `summary_of_organization` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `organization_summary` varchar(200) NOT NULL,
  `is_deleted` varchar(255) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summary_of_organization`
--

INSERT INTO `summary_of_organization` (`id`, `name`, `organization_summary`, `is_deleted`) VALUES
(1, 'Israt ', 'Meghna Group is one of the largest Bangladeshi conglomerates.The industries under this conglomerate include automobile, light engineering (bicycle), cement, packaging, textile etc. Meghna Gr', '0'),
(2, 'Jerin', 'This is a list of notable companies based in Bangladesh, grouped by their Industry Classification Benchmark sector. Small, medium and large family owned conglomerates dominate over Bangladesh''s $175', '0'),
(3, 'zakia', 'Orion Group[1] is one of the largest Bangladeshi industrial conglomerates.[2] The industries under this conglomerate include textiles, chemicals, pharmaceuticals, infrastructure development, agribusin', '1'),
(4, 'tajkia', 'Pride Group is a vertical textile group engaged in the manufacture and export of knitwear products to the European Union, the USA and Canada. The group is also engaged in production and marketing of s', '0'),
(5, 'Tasnia Mahmuda', 'Partex group is one of the largest Bangladeshi industrial conglomerates. The industries under this conglomerate include foods and beverages, steel, real estate, furniture, agribusiness, plastics, etc', '1'),
(6, 'Tamanna Naznin', '\r\nNasir Group is one of the largest Bangladeshi industrial conglomerates.[3] The industries under this conglomerate include industrial glass, tobacco, printing and packages, light engineering (light b', '0'),
(7, 'afrin', 'City Group (Bengali: à¦¸à¦¿à¦Ÿà¦¿ à¦—à§à¦°à§à¦ª ) is one of the largest Bangladeshi conglomerates. It began on 6 February 1972 as a mustard oil company venture under the name City Oil Mills. The fir', '0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `summary_of_organization`
--
ALTER TABLE `summary_of_organization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
